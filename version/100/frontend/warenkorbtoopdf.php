<?php 
global $smarty;

require_once(PFAD_ROOT . PFAD_INCLUDES . "tools.Global.php");

require_once($oPlugin->cFrontendPfad.'/includes/dompdf/autoload.inc.php');
use Dompdf\Dompdf;

if(isset($_GET['pdfdownload'])){
	$loggedin = false;
	//Schaue ob user eingeloggt ist um Daten aus der DB zu bekommen
	if($loggedin){
		$dompdf = new Dompdf();
		// Lese Daten über den User
		$smarty->assign('firma',$_POST['firma']);
		$smarty->assign('anrede',$_POST['anrede']);
		$smarty->assign('name',($_POST['name']));
		$smarty->assign('strasse',$_POST['strasse']);
		$smarty->assign('plz',$_POST['plz']);
		$smarty->assign('ort',($_POST['ort']));
		$smarty->assign('land',$_POST['land']);
	}else{	
		if(isset($_POST['firma']) && isset($_POST['anrede']) && isset($_POST['name']) && isset($_POST['strasse']) && isset($_POST['plz']) && isset($_POST['ort']) && isset($_POST['land'])){
			// Lese Daten über den User aus HTTP REQUEST
			$smarty->assign('firma',$_POST['firma']);
			$smarty->assign('anrede',$_POST['anrede']);
			$smarty->assign('name',($_POST['name']));
			$smarty->assign('strasse',$_POST['strasse']);
			$smarty->assign('plz',$_POST['plz']);
			$smarty->assign('ort',($_POST['ort']));
			$smarty->assign('land',$_POST['land']);
			$smarty->assign('frontendpath',$oPlugin->cFrontendPfad);
			$loggedin = true;
		}else{
			// Frag Daten über den User
			// Starte Javascript Popup zum Abfragen der Nutzerdaten,diese werden mit Post versendet  
			echo "lade javascript template<br><br>";
			
			//print($smarty-fetch("/home/julius/arbeit/jtl_test/includes/plugins/js_warenkorbToPdf/version/100/form2.tpl"));
			print($smarty->fetch($oPlugin->cFrontendPfad . "/template/form.tpl"));
		}
	}
	/* Zeige PDF Falls alle Daten vorhanden sind (aka (fake)eingeloggt)*/
	if($loggedin){
		$positionen = $smarty->_tpl_vars["Warenkorb"]->PositionenArr;
		$products = array();
		$count = 1;
		foreach ($positionen as $pos) {
			$preis = $pos->Artikel->Preise;
			$gesamt = $smarty->_tpl_vars["WarenkorbWarensumme"];
			$steuerpos =  $smarty->_tpl_vars["Steuerpositionen"];
			$product = array();						
			$product = array($pos->nAnzahl,$pos->cArtNr,implode($pos->cName),$preis->fUst,$pos->fPreis,($pos->fPreis * $pos->nAnzahl), ($pos->Artikel->cBeschreibung),$count++,$gesamt[1],$gesamt[0],$steuerpos[0]->fBetrag);
			array_push($products,$product);
		}
		$smarty->assign('products',$products);
		$smarty->assign('productsamount',sizeof($products));
	
		$html = $smarty->fetch($oPlugin->cFrontendPfad . "/template/template.tpl");
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->set_paper('a4', 'A4');
		$dompdf->render();
		$dompdf->stream('Anfrage_Auf_Office-4-Sale');		
	}	
}else{
	echo "<br><br><br><br>no download requested! call site with ?pdfdownload<br><br><br><br>";
}
?>
