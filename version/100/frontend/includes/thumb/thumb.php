<?php 
// Erstellt ein thumbnail eines Bildes 
// Ordner unter $speicherordner ben�tigt ggf. Schreibrechte CHMOD(777) 

// Parameter: 
// $imgfile: Pfad zur Bilddatei 
// $speicherordner: Ordner indem die Thumbnails gespeichert werden sollen 
// $filenameOnly: Soll nur der Dateiname als Name f�r Thumbnail verwendet werden, 
// ansonsten inkl. Pfad 
function thumbnail($imgfile, $speicherordner="./bilder/thumbnail/", $filenameOnly=true) 
   { 
   //Max. Gr��e des Thumbnail (H�he und Breite) 
   $thumbsize = 192; 
   
   //Dateiname erzeugen 
   $filename = basename($imgfile); 

   //F�gt den Pfad zur Datei dem Dateinamen hinzu 
   //Aus ordner/bilder/bild1.jpg wird dann ordner_bilder_bild1.jpg 
   if(!$filenameOnly) 
      { 
      $replace = array("/","\\","."); 
      $filename = str_replace($replace,"_",dirname($imgfile))."_".$filename; 
      } 

   //Schreibarbeit sparen 
   $ordner = $speicherordner; 
	
   //Speicherordner vorhanden 
   if(!is_dir($ordner)) {
	   echo "Speicherordner vorhanden<br>";
	   return false; 
   }
      

   //Wenn Datei schon vorhanden, kein Thumbnail erstellen 
   if(file_exists($ordner.$filename)){
	   echo "Datei schon vorhanden, kein Thumbnail erstellen<br>";
	   return $ordner.$filename;
   }
       

   //Ausgansdatei vorhanden? Wenn nicht, false zur�ckgeben 
   if(!file_exists($imgfile)) {
	   echo "Ausgansdatei vorhanden? Wenn nicht, false zur�ckgeben<br>";
	    return false; 
   }
     



   //Infos �ber das Bild 
   $endung = strrchr($imgfile,"."); 

   list($width, $height) = getimagesize($imgfile); 
   $imgratio=$width/$height; 
	if($width < $thumbsize){
		if($height < $thumbsize){
			//Bild ist kleiner als thumbsize
			$filepath = dirname($imgfile)."/".$filename;
			return $filepath;
		}
	}
   //Ist das Bild h�her als breit? 
   if($imgratio>1) { 
      $newwidth = $thumbsize; 
      $newheight = $thumbsize/$imgratio; 
    }else { 
      $newheight = $thumbsize; 
      $newwidth = $thumbsize*$imgratio; 
      } 

   //Bild erstellen 
   //Achtung: imagecreatetruecolor funktioniert nur bei bestimmten GD Versionen 
   //Falls ein Fehler auftritt, imagecreate nutzen 
   if(function_exists("imagecreatetruecolor")) 
     $thumb = imagecreatetruecolor($newwidth,$newheight);  
   else 
      $thumb = imagecreate ($newwidth,$newheight); 

   if(strtolower($endung) == ".jpg"){ 
      imageJPEG($thumb,$ordner."temp.jpg"); 
      $thumb = imagecreatefromjpeg($ordner."temp.jpg"); 

      $source = imagecreatefromjpeg($imgfile); 
	  imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height); 
    }else if(strtolower($endung) == ".gif"){ 
		imageGIF($thumb,$ordner."temp.gif"); 
		$thumb = imagecreatefromgif($ordner."temp.gif"); 

		$source = imagecreatefromgif($imgfile); 
		imagealphablending($thumb, FALSE);
		imagesavealpha($thumb, TRUE);
		imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height); 
    }else if(strtolower($endung) == ".png"){
		imagepng($thumb,$ordner."temp.png");

		$thumb = imagecreatefrompng($ordner."temp.png"); 
		
		$source = imagecreatefrompng($imgfile);	
		imagealphablending($thumb, FALSE);
		imagesavealpha($thumb, TRUE);
		imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height); 
	}

   //Bild speichern 
   if(strtolower($endung) == ".png") 
      imagepng($thumb,$ordner.$filename); 
   else if(strtolower($endung) == ".gif") 
      imagegif($thumb,$ordner.$filename); 
   else 
      imagejpeg($thumb,$ordner.$filename,100); 


   //Speicherplatz wieder freigeben 
   //ImageDestroy($thumb); 
   //ImageDestroy($source); 


   //Pfad zu dem Bild zur�ckgeben 
   return $ordner.$filename; 
   } 



?>