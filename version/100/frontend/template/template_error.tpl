<!DOCTYPE html>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
{literal}
  <style>
    @page { margin: 25px 25px; }
    footer { position: fixed; bottom: 20px; left: 0px; right: 0px; height: 150px; text-align:center;}
    p { page-break-after: always;font-family:Helvetica; padding-left: 50px;padding-right: 40px}
    p:last-child { page-break-after: never;font-family:Helvetica; font-size: small}
	td,th,h1,body { font-family:Helvetica; font-size: small}
  </style>
  {/literal}
</head>
<body>  
	<footer>
		<hr>
		<p>
			office-4-sale Büromöbel GmbH • Breitenbachstr. 24-29 • 13509 Berlin<br>
			Handelsregister: Amtsgericht Charlottenburg, HRB 151820 B • USt-IdNr.: DE815452436<br>
			Geschäftsführer: Alexander Potthoff & Nikolai Kuljasow<br>
			Commerzbank • Konto-Nr.: 972992200 • BLZ: 100 400 00<br>
			IBAN: DE36 1004 0000 0972 9922 00 • BIC: COBADEFFXXX<br>
			Hinweis gem. § 33 BDSG: Beteiligtendaten werden gespeichert.<br>  
		</p>
	</footer>
	<div id="sellerinfo" style="">
		<div id="anschreiben_kunde" style="margin-left:40px;margin-top:150px;min-width:70%;float:left">
			<p style=""><strong>office-4-sale, Breitenbachstr. 24-29, 13509 Berlin</strong><br>
			{$firma}<br>
			{$anrede} {$name}<br>
			{$strasse}<br>
			{$plz} {$ort}<br>
			{$land}</p>
		</div>	
		<div id="anschreiben_firma" style="margin-right:10px;margin-top:0pxwidth:30%;margin-left:70%">
			<img src="{$frontendpath}/template/img/o4s.png" alt="Office-4-Sale">
			office-4-sale Büromöbel GmbH<br>
			Nürnberger Straße 27<br>
			Standortanschrift: Düsseldorf<br>
			40599 Düsseldorf<br>
			Tel: 0211 - 999 154 56<br>
			Fax: 0211 - 999 154 52<br>
			Web: www.office-4-sale.de<br>
			AGB: www.office-4-sale.de/AGB<br>
			E-Mail: dl@office-4-sale.de
		</div>
		<main>
			<h1 style="text-align:center"><bold>Anfrage vom {$smarty.now|date_format}</bold></h1>
			<div id="product">
				<hr style="height:1.5px;background-color:black">
				<table style="width:100%">
					<tr>
						<th>Pos.</th>
						<th>Menge</th>
						<th>ArtNr</th>
						<th>Bezeichnung</th>
						<th>Ust.</th>
						<th>E-Preis</th>
						<th>G-Preis</th>
					</tr>										
				{foreach from=$products item=product}
					<tr>
						<td>{$product[7]}</td>
						<td>{$product[0]}</td>
						<td>{$product[1]}</td>
						<td>{$product[2]}<br>{$product[6]}</td>
						<td>{$product[3]}%</td>
						<td>{$product[4]}</td>
						<td>{$product[5]}</td>							
					</tr>
				{/foreach}
				</table>
					<hr>
					<div id="sum">
						<p style="page-break-after:always;text-align:right">
							 Gesamt Netto(19,00%)		{$product[8]}<br> 
							zzgl. 19,00% MwSt.			{$product[10]} EUR<br><br>
							<strong>Gesamt				{$product[9]}</strong>
						</p>
					</div>
			</div>		
			<p style="page-break-after:never">
				Anmerkungen / Vereinbarungen:<br><br>
				Zahlung: 7 Tage netto nach Rechnungsdatum<br><br>
				Lieferadresse falls nicht Abholung: Duratio GmbH, Herr Peter Heinze, Magdeburger Straße 23, 06112 Halle,<br> 0160
				91 31 31 59<br><br>
				Lieferung.<br><br>
				Lieferung / Versand erfolgt gemäß den aufgeführten Positionen mit der Art.Nr: 241 / 242<br>
			</p>
			<p style="page-break-after:always">
				Dieses ist lediglich ein unverbindliches Angebot für eine noch nicht erbrachte Leistung und berechtigt
				nicht zum Vorsteuerabzug! Das Angebot ist durch den Kunden zu unterzeichnen und an office-4-sale per
				Post, Mail oder Fax zurückzusenden. Dadurch unterbreitet der Kunde der Firma office-4-sale ein
				verbindliches Vertragsangebot. Im Falle der Annahme durch office-4-sale erfolgt die Auftragsbestätigung
				schnellstmöglich innerhalb von 5 Werktagen. Aufgrund eines begrenzten Warenvorrats wird diese
				Vorgehensweise durchgeführt.<br><br>
				Es gelten für alle Angebote von office-4-sale die auf der Homepage unter http://www.office-4-sale.de/AGB
				aufgeführten AGB, soweit in diesem Angebot nichts etwas abweichendes aufgeführt ist. Einzelheiten der
				Vertragsabwicklung sind folgende Punkte:<br><br>
			</p>
			<p style="page-break-after:auto">
				<strong>- Optionale Vertrage-/Montagekosten werden nach tatsächlich geleisteten Stunden vor Ort
				abgerechnet (Berechnungsgrundlage: office-4-sale berechnet einen Stundensatz von 30,00 € netto
				zzgl. MwSt. pro Mann, abweichend für Montage- und Vertragearbeiten von USM Haller Möbel wird
				ein Stundensatz von 40,00 € netto zzgl. Mwst. pro Mann berechnet.)</strong>
				- Der Rechnungsbetrag ist gemäß angegebener Zahlungsweise, spätestens bei Abholung / Lieferung
				ohne Abzug zur Zahlung fällig!<br>
				- Aushändigung der Ware nur gegen Vorlage der Auftragsbestätigung von office-4-sale!<br>
				- Dieses Angebot kann nur durch Unterschrift und Rücksendung an office-4-sale unterbreitet werden<br>
				- Die Lieferung erfolgt bis zur Bordsteinkante des Kunden an die angegebene Lieferadresse. Dabei hat der<br>
				Kunde Sorge zu tragen, dass die Anlieferung mit einem großen LkW (höhere Tonnage, bis zu 17 m lang)
				möglich ist. Der Kunde ist verpflichtet alle diesbezüglichen Angaben vorab zu liefern. Wenn eine Lieferung
				direkt an die Bordsteinkante wegen der örtlichen Gegebenheiten (beispielweise Fußgängerzone) nicht
				möglich ist, muß der Kunde eine abweichende, mögliche Lieferanschrift angeben!<br><br>
				Bitte prüfen Sie dieses Angebot und die von Ihnen angegebenen Daten (insbesondere die Firmierung!) auf
				Richtigkeit und Vollständigkeit vor Ihrer Bestätigung und Unterschrift. Nachträgliche Änderungen sind
				aufwändig und werden mit einer Bearbeitungspauschale von 20,- € netto berechnet, soweit diese nicht auf
				unserem Verschulden beruhen. Vielen Dank für Ihr Verständnis!<br><br>
				Hiermit bestätige ich _____________________ die AGB von office-4-sale auf der Homepage oder
				beigefügt erhalten und zur Kenntnis genommen zu haben, sowie vorstehendes Angebot verbindlich
				anzunehmen und die angegebenen Vor-Ort-Bedingungen zu bestätigen!<br><br>
			</p>

			<div id="signments" style="">
				<div id="leftsign" style="float:left;">
					<p>_______________________<br><br>
					Ort/Datum</p>
				</div>
				<div id="rightsign" style="float:right">
					<p>____________________________<br><br>
					Unterschrift/Firmenstempel</p>
				</div>
			</div>
		</main>
	</div>
</body>
</html>