	 {literal}
		 <script>		
		// Validating Empty Field
			function check_empty() {
				if (document.getElementById('firma').value == "" || document.getElementById('name').value == "" || document.getElementById('plz').value == "" || document.getElementById('ort').value == ""  || document.getElementById('land').value == ""|| document.getElementById('strasse').value == "") {
					alert("Bitte füllen Sie alle Felder aus!");
				} else {
					document.getElementById('form').submit();
				}
			}
			function check() {
				if (document.getElementById('firma').value == "" || document.getElementById('name').value == "" || document.getElementById('plz').value == "" || document.getElementById('ort').value == ""  || document.getElementById('land').value == ""|| document.getElementById('strasse').value == "") {
					return false;
				} else {
					return true;
				}
			}
			//Function To Display Popup
			function div_show() {
				document.getElementById('abc').style.display = "block";
			}
			//Function to Hide Popup
			function div_hide(){
				document.getElementById('abc').style.display = "none";
			}
			if (window.addEventListener){
				window.addEventListener("load", div_show, false);
			}else if (window.attachEvent){			
				window.attachEvent("onload", div_show);
			}else {
				window.onload = div_show;
			}
			document.addEventListener("keyup", function(event) {
				event.preventDefault();
				if (event.key == "Escape") {
					div_hide();
				}
				if (event.key == "Enter") {
					if(check()){
						document.getElementById('form').submit();
					}
				}
			});
    </script>
			{/literal}
<!-- Body Starts Here -->
<div style="overflow:hidden;" style="width:5000px">
	<div id="abc" style="width:100%;height:100%;opacity:.95;top:0;left:0;display:none;background-color:#313131;overflow:auto">
		<!-- Popup Div Starts Here -->
		<div id="popupContact" style="position:absolute;left:50%;top:17%;margin-left:-202px;font-family:'Raleway',sans-serif">
			<!-- Contact Us Form -->
			<form id="form" method="post" name="form" style="max-width:500px;min-width:250px;padding:10px 50px;border:2px solid gray;border-radius:10px;font-family:raleway;background-color:#fff">
				<p id="close" onclick ="div_hide()" style="position:absolute;right:5px;top:0px;font-size:30px;cursor:pointer">X</p>
				<h2 style="background-color:#FEFFED;padding:20px 35px;margin:-10px -50px;text-align:center;border-radius:10px 10px 0 0">Bitte geben Sie folgende Informationen an, um ein Angebot als PDF zu erhalten:</h2>
				<hr style="margin:10px -50px;border:0;border-top:1px solid #ccc">
				<input id="firma" name="firma" placeholder="Firma*" type="text" style="width:82%;padding:10px;margin-top:30px;border:1px solid #ccc;padding-left:40px;font-size:16px;font-family:raleway">
				<input id="anrede" name="anrede" placeholder="Anrede" type="text" style="width:82%;padding:10px;margin-top:30px;border:1px solid #ccc;padding-left:40px;font-size:16px;font-family:raleway">
				<input id="name" name="name" placeholder="Vor & Nachname" type="text" style="width:82%;padding:10px;margin-top:30px;border:1px solid #ccc;padding-left:40px;font-size:16px;font-family:raleway">
				<input id="strasse" name="strasse" placeholder="Straße & Hausnummer" type="text" style="width:82%;padding:10px;margin-top:30px;border:1px solid #ccc;padding-left:40px;font-size:16px;font-family:raleway">
				<input id="plz" name="plz" placeholder="Postleitzahl" type="text" style="width:82%;padding:10px;margin-top:30px;border:1px solid #ccc;padding-left:40px;font-size:16px;font-family:raleway">
				<input id="ort" name="ort" placeholder="Ort" type="text" style="width:82%;padding:10px;margin-top:30px;border:1px solid #ccc;padding-left:40px;font-size:16px;font-family:raleway">
				<input id="land" name="land" placeholder="Land" type="text" style="width:82%;padding:10px;margin-top:30px;border:1px solid #ccc;padding-left:40px;font-size:16px;font-family:raleway"><br><br><br>
				<a onclick="check_empty()" id="submit" style="text-decoration:none;width:100%;text-align:center;display:block;background-color:#FFBC00;color:#fff;border:1px solid #FFCB00;padding:10px 0;font-size:20px;cursor:pointer;border-radius:5px">Anfrage abschicken</a>
			</form>
		</div>
		<!-- Popup Div Ends Here -->
	</div>
</div>